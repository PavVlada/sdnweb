from app import app
from flask import render_template
from modules.helpers import get_data, read_json_from_file


@app.route('/', methods=['GET'])
def index():
    get_data()
    hosts = read_json_from_file('host_data.json')
    switches = read_json_from_file('switch_data.json')
    return render_template('index.html', switches=switches, hosts=hosts)