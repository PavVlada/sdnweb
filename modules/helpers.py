import os
import requests
import json
from requests.auth import HTTPBasicAuth


def data_path():
    return f"{os.path.dirname(os.path.abspath(__file__))}/../data"


def write_json_into_file(json_data, filename):
    path = data_path()
    file_path = f"{path}/{filename}"
    with open(file_path, "w") as file:
        json.dump(json_data, file, indent=2)


def read_json_from_file(filename):
    path = data_path()
    file_path = f"{path}/{filename}"
    with open(file_path, "r") as file:
        return json.load(file)


def request_data(urn, auth):
    res = requests.get(url=urn, auth=auth)
    return json.loads(res.text)


def get_topology(url, auth):
    topology_urn = f"{url}restconf/operational/network-topology:network-topology"
    json_topology = request_data(topology_urn, auth)
    return json_topology['network-topology']['topology'][0]['node']


def get_hosts_inf(nodes):
    json_hosts_inf = []

    for node in nodes:
        if "host" in node['node-id']:
            json_hosts_inf.append(node)
    
    return json_hosts_inf


def get_switch_list_from_topology(nodes):
    nodes = [node['node-id'] for node in nodes  if 'openflow' in node['node-id']]
    return nodes


def get_switch_inf(url, auth, nodes):
    json_switches_inf = []

    for node in nodes:
        node_urn = f"{url}restconf/operational/opendaylight-inventory:nodes/node/{node}"
        json_node_inf = request_data(node_urn, auth)

        # Deleting unnecessary keys

        del json_node_inf['node'][0]['flow-node-inventory:table']
        del json_node_inf['node'][0]['node-connector']

        json_switches_inf.append(json_node_inf['node'][0])

    return json_switches_inf


def get_data():
    auth = HTTPBasicAuth('admin', 'admin')
    ip_addr = "10.0.0.11"
    url = f"http://{ip_addr}:8181/"
    
    json_topology = get_topology(url, auth)

    json_hosts_info = get_hosts_inf(json_topology)

    list_switches = get_switch_list_from_topology(json_topology)
    print(sorted(list_switches))
    json_switches_info = get_switch_inf(url, auth, sorted(list_switches))
    
    write_json_into_file(json_hosts_info, 'host_data.json')
    write_json_into_file(json_switches_info, 'switch_data.json')
